package com.company;


import java.io.File;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class FileWalker {

    private static Collection<File> listFileTree(File dir) {
        Set<File> fileTree = new HashSet<>();
        if(dir==null||dir.listFiles()==null){
            return fileTree;
        }
        for (File entry : Objects.requireNonNull(dir.listFiles())) {
            if (entry.isFile()) fileTree.add(entry);
            else fileTree.addAll(listFileTree(entry));
        }
        return fileTree;
    }

    public static void main(String[] args) {
        File f = new File("D:\\.");

        FileWalker.listFileTree(f).stream().filter(s->s.toString().endsWith(".txt"))
                .forEach(System.out::println);
    }
}