package com.company;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

public class SimpleMultiFileReader {
        public static void main(String[] args) throws IOException
        {
            File dir = new File("D:\\YourFilesDirectory");

            // create object of PrintWriter for output file
            PrintWriter pw = new PrintWriter("D:\\texts\\output.txt");

            String[] fileNames = dir.list();

            assert fileNames != null;
            for (String fileName : fileNames) {
                System.out.println("Reading from " + fileName);

                File f = new File(dir, fileName);

                BufferedReader br = new BufferedReader(new FileReader(f));

                pw.println("Context of the file "+fileName);

                String r = "";
                String line = br.readLine();
                while (line != null) {
                    // Show in what line you want execute change
                    if(line.startsWith("t")){
                        //Sow what you what to change
                        r = line.replaceAll("old string", "new string");
                        pw.println(r);
                    }
                    line = br.readLine();
                }
                pw.flush();
            }
            System.out.println("Reading from all files" +
                    " in directory " + dir.getName() + " Completed");
        }
}
